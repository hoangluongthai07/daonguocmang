﻿
        int[] arr = { 1, 2, 3, 4, 5 };

        // Đảo ngược mảng
        for (int i = 0; i < arr.Length / 2; i++)
        {
            int temp = arr[i];
            arr[i] = arr[arr.Length - 1 - i];
            arr[arr.Length - 1 - i] = temp;
        }

        // In mảng đã đảo ngược
        foreach (int element in arr)
        {
            Console.WriteLine(element);
        }